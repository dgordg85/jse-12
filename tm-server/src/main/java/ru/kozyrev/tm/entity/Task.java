package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
public final class Task extends AbstractObject implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    private String projectId;
}

package ru.kozyrev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.exception.session.SessionSignatureException;

public final class SignatureUtil {
    @Nullable
    public static String sign(@Nullable final Object value) throws SessionSignatureException {
        return sign(value, ServerConst.SALT, ServerConst.CYCLE);
    }

    @Nullable
    public static String sign(
            @Nullable final Object value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) throws SessionSignatureException {
        try {
            @NotNull final ObjectMapper objectMapper =
                    new ObjectMapper();
            @NotNull final String json =
                    objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @NotNull
    public static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) throws SessionSignatureException {
        if (value == null || salt == null || cycle == null) {
            throw new SessionSignatureException();
        }
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getHash(salt + result + salt);
        }
        return result;
    }
}

package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public final class ConnectUtil {
    @NotNull
    public static final Connection getConnection() throws Exception {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "connection.properties");
        properties.load(fis);
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }
}

package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.SerializeType;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class PathUtil {
    public final static boolean createFile(
            @NotNull final String workDir,
            @NotNull final String filename
    ) throws Exception {
        Files.createDirectories(Paths.get(workDir));
        Files.deleteIfExists(Paths.get(workDir, filename));
        @NotNull final File file = new File(workDir + filename);
        return file.createNewFile();
    }

    @NotNull
    public final static String getPathAndCreateFile(@NotNull final SerializeType type, @NotNull final DataFormat dataFormat) throws Exception {
        @NotNull final String[] strArr = getPathArr(type, dataFormat);
        createFile(strArr[0], strArr[1]);
        return strArr[0] + strArr[1];
    }

    @NotNull
    public final static String getPath(@NotNull final SerializeType type, @NotNull final DataFormat dataFormat) {
        @NotNull final String[] strArr = getPathArr(type, dataFormat);
        return strArr[0] + strArr[1];
    }

    @NotNull
    private final static String[] getPathArr(@NotNull final SerializeType type, @NotNull final DataFormat dataFormat) {
        @NotNull final String[] strArr = new String[2];
        @NotNull final String workDir = System.getProperty("user.dir") + File.separator + "save" + File.separator;
        @NotNull final String filename = type.getName() + "-data" + dataFormat.getExpansion();
        strArr[0] = workDir;
        strArr[1] = filename;
        return strArr;
    }
}

package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.List;

public interface IProjectService extends IObjectService<Project> {
    @Nullable
    Project persist(@Nullable Project object) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<Project> object) throws Exception;

    @Nullable
    Project findOne(@Nullable String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws Exception;

    @Nullable
    Project persist(@Nullable Project object, @Nullable String userId) throws Exception;

    @Nullable
    Project merge(@Nullable Project object, @Nullable String userId) throws Exception;

    void remove(@Nullable String objectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Project> findWord(@Nullable String word, @Nullable String userId) throws Exception;

    @Nullable
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    void removeEntityByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@NotNull Column column, @NotNull Direction direction, @Nullable String userId) throws Exception;

    @NotNull
    ProjectRepository getRepository(@NotNull final Connection connection) throws Exception;
}


package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public interface IObjectRepository<T> extends IRepository<T> {
    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    T fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    List<T> getList(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;

    @Nullable
    T persist(@NotNull T object) throws Exception;

    @Nullable
    T merge(@NotNull T object) throws Exception;

    @Nullable
    T findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    List<T> findAll(@NotNull String userId) throws Exception;

    void remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    T findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<T> findWord(@NotNull String word, @NotNull String userId) throws Exception;
}

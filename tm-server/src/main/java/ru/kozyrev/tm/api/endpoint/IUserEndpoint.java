package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;

public interface IUserEndpoint {
    @Nullable
    User userFindOne(@Nullable Session session, @Nullable String userId) throws Exception;

    @Nullable
    User userMerge(@Nullable Session session, @Nullable User user) throws Exception;

    void userRemove(@Nullable final Session session, @Nullable final String userId) throws Exception;

    @Nullable
    User userPersist(@Nullable User user) throws Exception;

    @Nullable
    User userGetUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    void userUpdatePassword(@Nullable Session session, @Nullable String currentPassword, @Nullable String hashPassword) throws Exception;

    @NotNull
    String userGetUserRoleStr(@Nullable Session session) throws Exception;
}

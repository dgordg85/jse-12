package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Direction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public interface IProjectRepository extends IObjectRepository<Project> {
    @Nullable
    List<Project> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Project fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    List<Project> getList(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;

    @Nullable
    Project persist(@NotNull Project project) throws Exception;

    @Nullable
    Project merge(@NotNull Project project) throws Exception;

    @Nullable
    Project findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAll(@NotNull String userId) throws Exception;

    void remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    Project findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findWord(@NotNull String word, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAllByNum(@NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAllByDateStart(@NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAllByDateFinish(@NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAllByStatus(@NotNull Direction direction, @NotNull String userId) throws Exception;
}

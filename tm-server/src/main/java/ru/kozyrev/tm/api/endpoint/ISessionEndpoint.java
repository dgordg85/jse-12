package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

public interface ISessionEndpoint {
    @Nullable
    Session openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;
}

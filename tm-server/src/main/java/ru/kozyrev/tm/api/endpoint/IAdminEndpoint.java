package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IAdminEndpoint {
    void adminBinSave(@Nullable Session session) throws Exception;

    void adminBinLoad(@Nullable Session session) throws Exception;

    void adminJsonLoadFasterXML(@Nullable Session session) throws Exception;

    void adminJsonSaveFasterXML(@Nullable Session session) throws Exception;

    void adminXmlLoadFasterXML(@Nullable Session session) throws Exception;

    void adminXmlSaveFasterXML(@Nullable Session session) throws Exception;

    void adminJsonSaveJaxB(@Nullable Session session) throws Exception;

    void adminJsonLoadJaxB(@Nullable Session session) throws Exception;

    void adminXmlSaveJaxB(@Nullable Session session) throws Exception;

    void adminXmlLoadJaxB(@Nullable Session session) throws Exception;

    @NotNull
    String adminGetDocumentStatusStr(@Nullable String value) throws Exception;

    @NotNull
    String adminUserGetRoleTypeStr(@Nullable String value) throws Exception;

    void taskPersistList(@Nullable List<Task> tasks, @Nullable Session session) throws Exception;

    @Nullable
    List<Task> taskFindAll(@Nullable Session session) throws Exception;

    void taskRemoveAll(@Nullable Session session) throws Exception;

    @Nullable
    List<Project> projectFindAll(@Nullable Session session) throws Exception;

    void projectRemoveAll(@Nullable Session session) throws Exception;

    void projectPersistList(@Nullable Session session, @Nullable List<Project> projects) throws Exception;

    void userRemoveAll(@Nullable Session session) throws Exception;

    @Nullable
    List<User> userFindAll(@Nullable Session session) throws Exception;

    void userPersistList(@Nullable Session session, @Nullable List<User> users) throws Exception;
}

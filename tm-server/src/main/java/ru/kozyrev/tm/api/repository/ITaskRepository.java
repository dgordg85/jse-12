package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Direction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository extends IObjectRepository<Task> {
    @Nullable
    List<Task> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Task fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    List<Task> getList(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;

    @Nullable
    Task persist(@NotNull Task task) throws Exception;

    @Nullable
    Task merge(@NotNull Task task) throws Exception;

    @Nullable
    Task findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String userId) throws Exception;

    void remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    Task findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findWord(@NotNull String word, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String projectId, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String projectId, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAllByNum(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAllByDateStart(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAllByDateFinish(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAllByStatus(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId) throws Exception;
}

package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.List;

public interface IUserService extends IEntityService<User> {
    @Nullable
    List<User> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    @Nullable
    User persist(@Nullable User user) throws Exception;

    @Nullable
    User persistSaltPass(@Nullable User user) throws Exception;

    @Nullable
    User merge(@Nullable User user) throws Exception;

    @Nullable
    User mergeSaltPass(@Nullable final User user) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void persist(@Nullable List<User> users) throws Exception;

    @Nullable
    User getUserByLogin(@Nullable String login) throws Exception;

    void updateUserPassword(@Nullable String passC, @Nullable String hashPass, @Nullable String userId) throws Exception;

    @Nullable
    String isPasswordTrue(@NotNull User user, @Nullable String hashPassword) throws Exception;

    @NotNull
    String getUserRoleStr(@Nullable String userId) throws Exception;

    @NotNull
    User saltPass(@Nullable User user) throws Exception;

    @NotNull
    UserRepository getRepository(@NotNull Connection connection) throws Exception;
}

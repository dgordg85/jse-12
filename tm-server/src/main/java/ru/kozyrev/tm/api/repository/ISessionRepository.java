package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

import java.sql.ResultSet;
import java.util.List;

public interface ISessionRepository extends IEntityRepository<Session> {
    @Nullable
    List<Session> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Session fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    Session persist(@NotNull Session session) throws Exception;

    @Nullable
    Session merge(@NotNull Session session) throws Exception;

    @Nullable
    Session findOne(@NotNull String id) throws Exception;

    void remove(@NotNull final String id) throws Exception;

    @Nullable
    Session getSessionByUserId(String userId) throws Exception;
}

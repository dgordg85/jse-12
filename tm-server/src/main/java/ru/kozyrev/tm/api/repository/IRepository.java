package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public interface IRepository<T> {
    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    T fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    T fetch(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;

    @Nullable
    List<T> getList(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;
}

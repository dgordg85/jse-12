package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public interface IUserRepository extends IEntityRepository<User> {
    @Nullable
    List<User> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User fetch(@NotNull ResultSet rs) throws Exception;

    @Nullable
    List<User> getList(@NotNull ResultSet rs, @NotNull PreparedStatement statement) throws Exception;

    @Nullable
    User persist(@NotNull User user) throws Exception;

    @Nullable
    User merge(@NotNull User user) throws Exception;

    @Nullable
    User findOne(@NotNull String id) throws Exception;

    void remove(@NotNull String id) throws Exception;

    @Nullable
    User getUserByLogin(@NotNull String login) throws Exception;
}
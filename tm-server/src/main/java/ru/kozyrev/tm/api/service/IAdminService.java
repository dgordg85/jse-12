package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminService {
    void binSave() throws Exception;

    void binLoad(@NotNull String sessionId) throws Exception;

    void jsonLoadFasterXML(@NotNull String sessionId) throws Exception;

    void jsonSaveFasterXML() throws Exception;

    void jsonSaveJaxB() throws Exception;

    void jsonLoadJaxB(@NotNull String sessionId) throws Exception;

    void xmlLoadFasterXML(@NotNull String sessionId) throws Exception;

    void xmlSaveFasterXML() throws Exception;

    void xmlSaveJaxB() throws Exception;

    void xmlLoadJaxB(@NotNull String sessionId) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @NotNull String getDocumentStatusStr(@Nullable String value) throws Exception;

    @NotNull String getRoleTypeStr(@Nullable String value) throws Exception;
}

package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface ITaskEndpoint {
    @Nullable
    Task taskFindOneAuth(@Nullable Session session, @Nullable String taskId) throws Exception;

    @Nullable
    List<Task> taskFindAllAuth(@Nullable Session session) throws Exception;

    void taskRemoveAuth(@Nullable Session session, @Nullable String taskId) throws Exception;

    void taskRemoveAllAuth(@Nullable Session session) throws Exception;

    @Nullable
    String taskGetEntityIdByShortLink(@Nullable Session session, @Nullable String num) throws Exception;

    @Nullable
    List<Task> taskFindWord(@Nullable Session session, @NotNull String word) throws Exception;

    @Nullable
    Task taskPersistAuth(@Nullable Session session, @Nullable Task task) throws Exception;

    @Nullable
    Task taskMergeAuth(@Nullable Session session, @Nullable Task task) throws Exception;

    void taskRemoveAllByProjectAuth(@Nullable Session session, @Nullable String projectId) throws Exception;

    void taskRemoveAllByProjectNumAuth(@Nullable Session session, @Nullable String projectNum) throws Exception;

    @Nullable
    List<Task> taskFindAllByProjectAuth(@Nullable Session session, @Nullable String projectId) throws Exception;

    @Nullable
    List<Task> taskFindAllByProjectAuthSort(@Nullable Session session, @Nullable String projectId, @NotNull Column column, @NotNull Direction direction) throws Exception;

    void taskRemoveByShortLink(@Nullable Session session, @Nullable String num) throws Exception;
}

package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.repository.SessionRepository;

import java.sql.Connection;

public interface ISessionService extends IEntityService<Session> {
    @Nullable
    Session persist(@Nullable Session session) throws Exception;

    @Nullable
    Session openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    boolean validateSession(@Nullable Session session) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @Nullable
    Session findOne(@Nullable String id) throws Exception;

    @NotNull
    SessionRepository getRepository(@NotNull Connection connection) throws Exception;
}

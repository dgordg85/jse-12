package ru.kozyrev.tm.exception.entity;

public final class EntityException extends Exception {
    public EntityException() {
        super("Entity work error!");
    }
}

package ru.kozyrev.tm.exception.data;

public final class NoDataException extends Exception {
    public NoDataException() {
        super("Can't find save file!");
    }
}

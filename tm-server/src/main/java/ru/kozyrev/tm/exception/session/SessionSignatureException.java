package ru.kozyrev.tm.exception.session;

public final class SessionSignatureException extends Exception {
    public SessionSignatureException() {
        super("Session can't be signing!");
    }
}

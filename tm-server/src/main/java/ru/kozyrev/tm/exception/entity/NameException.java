package ru.kozyrev.tm.exception.entity;

public final class NameException extends Exception {
    public NameException() {
        super("Wrong name!");
    }
}

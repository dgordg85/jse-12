package ru.kozyrev.tm.exception.user;

public final class UserLoginEmptyException extends Exception {
    public UserLoginEmptyException() {
        super("Login is empty!");
    }
}

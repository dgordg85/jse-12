package ru.kozyrev.tm.exception.session;

public final class SessionCloseException extends Exception {
    public SessionCloseException() {
        super("Session can't be close!");
    }
}

package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IProjectEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/projectService?wsdl";

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public Project projectFindOneAuth(
            @Nullable final Session session,
            @Nullable final String id
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().findOne(id, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> projectFindAllAuth(@Nullable final Session session) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    public void projectRemoveAuth(
            @Nullable final Session session,
            @Nullable final String projectId
    ) throws Exception {
        checkSession(session);
        serviceLocator.getProjectService().remove(projectId, session.getUserId());
    }

    @WebMethod
    @Override
    public void projectRemoveAllAuth(@Nullable final Session session) throws Exception {
        checkSession(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public String projectGetEntityIdByShortLink(
            @Nullable final Session session,
            @Nullable final String num
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().getEntityIdByShortLink(num, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> projectFindWord(
            @Nullable final Session session,
            @NotNull final String word
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().findWord(word, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Project projectPersistAuth(
            @Nullable final Session session,
            @Nullable final Project project
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().persist(project, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Project projectMergeAuth(
            @Nullable final Session session,
            @Nullable final Project project
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().merge(project, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> projectFindAllBySorting(
            @Nullable final Session session,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getProjectService().findAll(column, direction, session.getUserId());
    }

    @WebMethod
    @Override
    public void projectRemoveByShortLink(
            @Nullable final Session session,
            @Nullable final String num
    ) throws Exception {
        checkSession(session);
        serviceLocator.getProjectService().removeEntityByShortLink(num, session.getUserId());
    }
}

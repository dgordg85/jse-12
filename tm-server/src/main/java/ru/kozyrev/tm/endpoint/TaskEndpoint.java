package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ITaskEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/taskService?wsdl";

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public Task taskFindOneAuth(
            @Nullable final Session session,
            @Nullable final String id
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().findOne(id, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> taskFindAllAuth(@Nullable final Session session) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    public void taskRemoveAuth(
            @Nullable final Session session,
            @Nullable final String taskId
    ) throws Exception {
        checkSession(session);
        serviceLocator.getTaskService().remove(taskId, session.getUserId());
    }

    @WebMethod
    @Override
    public void taskRemoveAllAuth(@Nullable final Session session) throws Exception {
        checkSession(session);
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public String taskGetEntityIdByShortLink(
            @Nullable final Session session,
            @Nullable final String num
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().getEntityIdByShortLink(num, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> taskFindWord(
            @Nullable final Session session,
            @NotNull final String word
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().findWord(word, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Task taskPersistAuth(
            @Nullable final Session session,
            @Nullable final Task task
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().persist(task, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Task taskMergeAuth(
            @Nullable final Session session,
            @Nullable final Task task
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().merge(task, session.getUserId());
    }

    @WebMethod
    @Override
    public void taskRemoveAllByProjectAuth(
            @Nullable final Session session,
            @Nullable final String projectId
    ) throws Exception {
        checkSession(session);
        serviceLocator.getTaskService().removeAll(projectId, session.getUserId());
    }

    @WebMethod
    @Override
    public void taskRemoveAllByProjectNumAuth(
            @Nullable final Session session,
            @Nullable final String projectNum
    ) throws Exception {
        checkSession(session);
        serviceLocator.getTaskService().removeAllByProjectNum(projectNum, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> taskFindAllByProjectAuth(
            @Nullable final Session session,
            @Nullable final String projectId
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().findAll(projectId, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> taskFindAllByProjectAuthSort(
            @Nullable final Session session,
            @Nullable final String projectId,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getTaskService().findAll(projectId, column, direction, session.getUserId());
    }

    @WebMethod
    @Override
    public void taskRemoveByShortLink(
            @Nullable final Session session,
            @Nullable final String num
    ) throws Exception {
        checkSession(session);
        serviceLocator.getTaskService().removeEntityByShortLink(num, session.getUserId());
    }
}

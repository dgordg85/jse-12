package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IAdminEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/adminService?wsdl";

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public void adminBinSave(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().binSave();
    }

    @WebMethod
    @Override
    public void adminBinLoad(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().binLoad(session.getId());
    }

    @WebMethod
    @Override
    public void adminJsonLoadFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadFasterXML(session.getId());
    }

    @WebMethod
    @Override
    public void adminJsonSaveFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveFasterXML();
    }

    @WebMethod
    @Override
    public void adminXmlLoadFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadFasterXML(session.getId());
    }

    @WebMethod
    @Override
    public void adminXmlSaveFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveFasterXML();
    }

    @WebMethod
    @Override
    public void adminJsonSaveJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveJaxB();
    }

    @WebMethod
    @Override
    public void adminJsonLoadJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadJaxB(session.getId());
    }

    @WebMethod
    @Override
    public void adminXmlSaveJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveJaxB();
    }

    @WebMethod
    @Override
    public void adminXmlLoadJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadJaxB(session.getId());
    }

    @WebMethod
    public void projectPersistList(
            @Nullable final Session session,
            @Nullable final List<Project> projects
    ) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getProjectService().persist(projects);
    }

    @WebMethod
    @Override
    public void projectRemoveAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getProjectService().removeAll();
    }

    @WebMethod
    @Nullable
    @Override
    public List<Project> projectFindAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        return serviceLocator.getProjectService().findAll();
    }

    @WebMethod
    @Override
    public void taskPersistList(
            @Nullable final List<Task> tasks,
            @Nullable final Session session
    ) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getTaskService().persist(tasks);
    }

    @WebMethod
    @Nullable
    @Override
    public List<Task> taskFindAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    @Override
    public void taskRemoveAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getTaskService().removeAll();
    }

    @WebMethod
    @Override
    public void userPersistList(
            @Nullable final Session session,
            @Nullable final List<User> users
    ) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getUserService().persist(users);
    }

    @WebMethod
    @Nullable
    @Override
    public List<User> userFindAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        return serviceLocator.getUserService().findAll();
    }


    @WebMethod
    @Override
    public void userRemoveAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getUserService().removeAll();
    }

    @WebMethod
    @NotNull
    @Override
    public String adminGetDocumentStatusStr(@Nullable final String value) throws Exception {
        return serviceLocator.getAdminService().getDocumentStatusStr(value);
    }

    @WebMethod
    @NotNull
    @Override
    public String adminUserGetRoleTypeStr(@Nullable final String value) throws Exception {
        return serviceLocator.getAdminService().getRoleTypeStr(value);
    }

    private final void checkForbidden(@NotNull final Session session) throws Exception {
        if (session.getRoleType() == null) {
            throw new AccessForbiddenException();
        }
        if (!session.getRoleType().equals(RoleType.ADMIN)) {
            throw new AccessForbiddenException();
        }
    }
}

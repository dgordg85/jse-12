package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IUserEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/userService?wsdl";

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public User userFindOne(
            @Nullable final Session session,
            @Nullable final String userId
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().findOne(userId);
    }

    @WebMethod
    @Nullable
    @Override
    public User userPersist(@Nullable final User user) throws Exception {
        return serviceLocator.getUserService().persistSaltPass(user);
    }

    @WebMethod
    @Nullable
    @Override
    public User userMerge(
            @Nullable final Session session,
            @Nullable final User user
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().mergeSaltPass(user);
    }

    @WebMethod
    @Override
    public void userRemove(
            @Nullable final Session session,
            @Nullable final String userId
    ) throws Exception {
        checkSession(session);
        serviceLocator.getUserService().remove(userId);
    }

    @WebMethod
    @Nullable
    @Override
    public User userGetUserByLogin(
            @Nullable final Session session,
            @Nullable final String login
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().getUserByLogin(login);
    }

    @WebMethod
    @Override
    public void userUpdatePassword(
            @Nullable final Session session,
            @Nullable final String currentPassword,
            @Nullable final String newPassword
    ) throws Exception {
        checkSession(session);
        serviceLocator.getUserService().updateUserPassword(currentPassword, newPassword, session.getUserId());
    }

    @WebMethod
    @NotNull
    public String userGetUserRoleStr(@Nullable final Session session) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().getUserRoleStr(session.getUserId());
    }
}

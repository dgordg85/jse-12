package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.util.ConnectUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    protected ServiceLocator serviceLocator;

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws Exception;

    @Nullable
    @Override
    public final List<T> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @Nullable final List<T> entities = getRepository(connection).findAll();
        connection.close();
        return entities;
    }

    @Override
    public final void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).removeAll();
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(@Nullable final List<T> entities) throws Exception {
        if (entities == null) {
            throw new NoDataException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            for (final T entity : entities) {
                if (entity == null) {
                    continue;
                }
                this.persist(entity);
            }
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @Override
    public final void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract IRepository<T> getRepository(@NotNull final Connection connection);

    @NotNull
    public final Connection getConnection() throws Exception {
        return ConnectUtil.getConnection();
    }
}

package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class ProjectService extends AbstractObjectService<Project> implements IProjectService {
    @Nullable
    @Override
    public final Project persist(@Nullable final Project project) throws Exception {
        if (project == null) {
            throw new EmptyEntityException();
        }
        if (project.getId() == null || project.getId().isEmpty()) {
            project.setId(UUID.randomUUID().toString());
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        if (project.getStatus() == null) {
            project.setStatus(DocumentStatus.PLANNED);
        }
        @Nullable Project result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).persist(project);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final Project merge(
            @Nullable final Project project,
            @Nullable final String userId
    ) throws Exception {
        if (project == null) {
            throw new ServiceFailException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getId() == null) {
            throw new EntityException();
        }
        @Nullable final Project projectUpdate = this.findOne(project.getId(), userId);
        if (projectUpdate == null) {
            return persist(project, userId);
        }
        if (project.getName() != null && !project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (project.getDescription() != null && !project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        if (project.getDateStart() != null) {
            projectUpdate.setDateStart(project.getDateStart());
        }
        if (project.getDateFinish() != null) {
            projectUpdate.setDateFinish(project.getDateFinish());
        }
        if (project.getStatus() != null) {
            projectUpdate.setStatus(project.getStatus());
        }
        @Nullable Project result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).merge(projectUpdate);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final List<Project> findAll(
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final List<Project> list;
        @NotNull final Connection connection = getConnection();
        switch (column) {
            case DATE_BEGIN:
                list = getRepository(connection).findAllByDateStart(direction, userId);
                break;
            case DATE_FINISH:
                list = getRepository(connection).findAllByDateFinish(direction, userId);
                break;
            case STATUS:
                list = getRepository(connection).findAllByStatus(direction, userId);
                break;
            default:
                list = getRepository(connection).findAllByNum(direction, userId);
        }
        connection.close();
        return list;
    }

    @NotNull
    public final ProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }
}

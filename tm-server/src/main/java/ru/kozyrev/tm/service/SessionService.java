package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ISessionService;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.session.SessionCloseException;
import ru.kozyrev.tm.exception.session.SessionCreateException;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.exception.session.SessionSignatureException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.repository.SessionRepository;
import ru.kozyrev.tm.util.HashUtil;
import ru.kozyrev.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

public final class SessionService extends AbstractEntityService<Session> implements ISessionService {
    @Nullable
    @Override
    public final Session persist(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionCreateException();
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            session.setId(UUID.randomUUID().toString());
        }
        if (session.getRoleType() == null) {
            throw new EmptyEntityException();
        }
        if (session.getUserId() == null || session.getUserId().isEmpty()) {
            throw new EmptyEntityException();
        }
        if (session.getTimestamp() == null) {
            throw new EmptyEntityException();
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            throw new EmptyEntityException();
        }
        @Nullable Session result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).persist(session);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final Session merge(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionNotValidException();
        }
        if (session.getId() == null) {
            throw new EmptyEntityException();
        }
        @Nullable final Session sessionDB = findOne(session.getId());
        if (sessionDB == null) {
            return this.persist(session);
        }
        if (session.getTimestamp() != null && !session.getId().isEmpty()) {
            sessionDB.setTimestamp(session.getTimestamp());
        }
        if (session.getRoleType() != null) {
            sessionDB.setRoleType(session.getRoleType());
        }
        if (session.getUserId() != null && session.getUserId().isEmpty()) {
            sessionDB.setUserId(session.getUserId());
        }
        @NotNull final Session sessionSign = signSession(sessionDB);
        @Nullable Session result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).merge(sessionSign);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;

    }

    @Nullable
    @Override
    public final Session openSession(
            @Nullable final String login,
            @Nullable final String hashPassword
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (isPassEmpty(hashPassword)) {
            throw new UserPasswordEmptyException();
        }
        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        @NotNull final String saltHashPassword = HashUtil.getCycleHash(hashPassword);
        if (!saltHashPassword.equals(user.getPasswordHash())) {
            throw new UserPasswordWrongException();
        }
        @Nullable final Session session = getSessionByUserId(user.getId());
        if (session != null) {
            return session;
        }
        @NotNull final Session newSession = createSession(user);
        return this.persist(newSession);
    }

    @Override
    public final boolean validateSession(@Nullable final Session session) throws Exception {
        return isSessionExist(session) && isSessionAlive(session);
    }

    private final boolean isSessionExist(@Nullable final Session session) throws Exception {
        if (session == null) {
            return false;
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            throw new SessionNotValidException();
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final Session serverSession = getRepository(connection).findOne(session.getId());
        connection.close();
        if (serverSession == null) {
            return false;
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            return false;
        }
        return serverSession.equals(session);
    }

    private final boolean isSessionAlive(@Nullable final Session session) throws Exception {
        if (session == null) {
            return false;
        }
        if (session.getTimestamp() == null) {
            throw new SessionNotValidException();
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            throw new SessionNotValidException();
        }
        final long sessionTime = System.currentTimeMillis() - session.getTimestamp();
        if (ServerConst.SESSION_LIFETIME < sessionTime) {
            @NotNull final Connection connection = getConnection();
            getRepository(connection).remove(session.getId());
            connection.close();
            return false;
        }
        return true;
    }

    @Override
    public final void closeSession(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionCloseException();
        }
        if (!isSessionExist(session)) {
            throw new SessionNotValidException();
        }
        @Nullable final String sessionId = session.getId();
        if (sessionId == null) {
            throw new SessionCloseException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).remove(sessionId);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @NotNull
    private final Session createSession(@NotNull final User user) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Session sessionSign = signSession(session);
        return sessionSign;
    }

    @NotNull
    private final Session signSession(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionSignatureException();
        }
        @Nullable final String signature = SignatureUtil.sign(session);
        if (signature == null) {
            throw new SessionSignatureException();
        }
        session.setSignature(signature);
        return session;
    }

    @Nullable
    private final Session getSessionByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            return null;
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final Session session = getRepository(connection).getSessionByUserId(userId);
        connection.close();
        return session;
    }

    @NotNull
    public final SessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }
}

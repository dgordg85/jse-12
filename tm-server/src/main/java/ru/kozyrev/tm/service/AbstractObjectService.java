package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IObjectRepository;
import ru.kozyrev.tm.api.service.IObjectService;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.util.StringUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectService<T extends AbstractObject> extends AbstractService<T> implements IObjectService<T> {
    @Nullable
    @Override
    public abstract T persist(@Nullable final T object) throws Exception;

    @Nullable
    @Override
    public final T persist(
            @Nullable final T object,
            @Nullable final String userId
    ) throws Exception {
        if (object == null) {
            throw new EmptyEntityException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        object.setUserId(userId);
        return this.persist(object);
    }

    @Nullable
    @Override
    public abstract T merge(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public final T findOne(
            @Nullable final String id,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (id == null || id.isEmpty()) {
            return null;
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final T object = getRepository(connection).findOne(id, userId);
        connection.close();
        return object;
    }

    @Nullable
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final List<T> object = getRepository(connection).findAll(userId);
        connection.close();
        return object;
    }

    @Override
    public final void remove(@Nullable final String objectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (objectId == null || objectId.isEmpty()) {
            throw new EmptyEntityException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).remove(objectId, userId);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).removeAll(userId);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public List<T> findWord(
            @Nullable final String word,
            @Nullable final String userId
    ) throws Exception {
        if (word == null || word.isEmpty()) {
            return null;
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final List<T> object = getRepository(connection).findWord(word, userId);
        connection.close();
        return object;
    }

    @Nullable
    @Override
    public final String getEntityIdByShortLink(
            @Nullable final String num,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        final int shortLink = StringUtil.parseToInt(num);
        @NotNull final Connection connection = getConnection();
        @Nullable final T entity = getRepository(connection).findOneByShortLink(shortLink, userId);
        connection.close();
        if (entity == null) {
            throw new IndexException();
        }
        return entity.getId();
    }

    @Override
    public void removeEntityByShortLink(
            @Nullable final String num,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final String entityId = getEntityIdByShortLink(num, userId);
        if (entityId == null) {
            throw new IndexException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).remove(entityId, userId);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @NotNull
    public abstract IObjectRepository<T> getRepository(@NotNull final Connection connection);
}

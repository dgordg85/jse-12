package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IEntityRepository;
import ru.kozyrev.tm.api.service.IEntityService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

@NoArgsConstructor
public abstract class AbstractEntityService<T extends AbstractEntity> extends AbstractService<T> implements IEntityService<T> {
    @Nullable
    @Override
    public final T findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final T session = getRepository(connection).findOne(id);
        connection.close();
        return session;
    }

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@Nullable final T entity) throws Exception;

    @Override
    public final void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            getRepository(connection).remove(id);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    protected final boolean isPassEmpty(@Nullable final String hashPassword) {
        return hashPassword == null || hashPassword.isEmpty() || hashPassword.equals(HashUtil.EMPTY_PASSWORD);
    }

    @NotNull
    public abstract IEntityRepository<T> getRepository(@NotNull final Connection connection);
}

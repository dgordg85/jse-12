package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.user.*;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

@NoArgsConstructor
public final class UserService extends AbstractEntityService<User> implements IUserService {
    @Nullable
    @Override
    public final User persist(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getId() == null || user.getId().isEmpty()) {
            user.setId(UUID.randomUUID().toString());
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        @Nullable final User userInDB = getUserByLogin(user.getLogin());
        if (userInDB != null) {
            throw new UserLoginTakenException();
        }
        if (isPassEmpty(user.getPasswordHash())) {
            throw new UserPasswordEmptyException();
        }
        if (user.getRoleType() == null) {
            user.setRoleType(RoleType.USER);
        }
        @Nullable User result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).persist(user);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final User persistSaltPass(@Nullable final User user) throws Exception {
        @NotNull final User userSaltPass = saltPass(user);
        return this.persist(userSaltPass);
    }

    @Nullable
    @Override
    public final User merge(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getId() == null || user.getId().isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final User userUpdate = this.findOne(user.getId());
        if (userUpdate == null) {
            return this.persist(user);
        }
        if (user.getLogin() != null && !user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }
        if (user.getPasswordHash() != null && !isPassEmpty(user.getPasswordHash())) {
            userUpdate.setPasswordHash(user.getPasswordHash());
        }
        if (user.getRoleType() != null) {
            userUpdate.setRoleType(user.getRoleType());
        }
        @Nullable User result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).merge(userUpdate);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final User mergeSaltPass(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EntityException();
        }
        if (!isPassEmpty(user.getPasswordHash())) {
            saltPass(user);
        }
        return this.merge(user);
    }

    @Nullable
    @Override
    public final User getUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) {
            return null;
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final User user = getRepository(connection).getUserByLogin(login);
        connection.close();
        return user;
    }

    @Override
    public final void updateUserPassword(
            @Nullable final String currentHashPassword,
            @Nullable final String hashPassword,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final User user = this.findOne(userId);
        if (user == null) {
            throw new EntityException();
        }
        @Nullable final String cyclePass = isPasswordTrue(user, currentHashPassword);
        if (cyclePass == null) {
            throw new UserPasswordWrongException();
        }
        @NotNull final String newCyclePass = HashUtil.getCycleHash(hashPassword);
        user.setPasswordHash(newCyclePass);
        @NotNull final Connection connection = getConnection();
        getRepository(connection).merge(user);
        connection.close();
    }

    @Nullable
    @Override
    public final String isPasswordTrue(
            @NotNull final User user,
            @Nullable final String hashPassword
    ) throws Exception {
        if (isPassEmpty(hashPassword)) {
            return null;
        }
        @NotNull final String cycleHashPassword = HashUtil.getCycleHash(hashPassword);
        if (cycleHashPassword.equals(user.getPasswordHash())) {
            return cycleHashPassword;
        }
        return null;
    }

    @NotNull
    @Override
    public final String getUserRoleStr(@Nullable final String userId) throws Exception {
        @Nullable final User user = findOne(userId);
        if (user == null) {
            throw new EntityException();
        }
        if (user.getRoleType() == null) {
            throw new EntityException();
        }
        return user.getRoleType().getDisplayName();
    }

    @NotNull
    @Override
    public final User saltPass(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        @Nullable final String userPass = user.getPasswordHash();
        if (isPassEmpty(userPass)) {
            throw new UserPasswordEmptyException();
        }
        user.setPasswordHash(HashUtil.getCycleHash(userPass));
        return user;
    }

    @NotNull
    public final UserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }
}

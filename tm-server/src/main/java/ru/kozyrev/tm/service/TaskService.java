package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class TaskService extends AbstractObjectService<Task> implements ITaskService {
    @Nullable
    @Override
    public final Task persist(@Nullable final Task task) throws Exception {
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            task.setId(UUID.randomUUID().toString());
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        if (task.getStatus() == null) {
            task.setStatus(DocumentStatus.PLANNED);
        }
        @Nullable Task result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).persist(task);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final Task merge(
            @Nullable final Task task,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            throw new EntityException();
        }
        @Nullable final Task taskUpdate = this.findOne(task.getId(), userId);
        if (taskUpdate == null) {
            return persist(task, userId);
        }
        if (task.getName() != null && !task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (task.getProjectId() != null && !task.getProjectId().isEmpty()) {
            taskUpdate.setProjectId(task.getProjectId());
        }
        if (task.getDescription() != null && !task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        if (task.getStatus() != null) {
            taskUpdate.setStatus(task.getStatus());
        }
        @Nullable Task result;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            result = getRepository(connection).merge(taskUpdate);
            connection.commit();
        } catch (final SQLException e) {
            connection.rollback();
            result = null;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final Connection connection = getConnection();
        @Nullable final List<Task> result = getRepository(connection).findAll(projectId, userId);
        connection.close();
        return result;
    }

    @Override
    public final void removeAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final Connection connection = getConnection();
        getRepository(connection).removeAll(projectId, userId);
        connection.close();
    }

    @Override
    public final void removeAllByProjectNum(
            @Nullable final String projectNum,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        this.removeAll(projectId, userId);
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @Nullable final List<Task> list;
        @NotNull final Connection connection = getConnection();
        switch (column) {
            case DATE_BEGIN:
                list = getRepository(connection).findAllByDateStart(projectId, direction, userId);
                break;
            case DATE_FINISH:
                list = getRepository(connection).findAllByDateFinish(projectId, direction, userId);
                break;
            case STATUS:
                list = getRepository(connection).findAllByStatus(projectId, direction, userId);
                break;
            default:
                list = getRepository(connection).findAllByNum(projectId, direction, userId);
        }
        connection.close();
        return list;
    }

    @NotNull
    public final TaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }
}

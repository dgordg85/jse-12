package ru.kozyrev.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ServerConst {
    @NotNull
    public final static String SALT = "K6@8ll&stv0iej";

    @NotNull
    public final static Integer CYCLE = 127;

    @NotNull
    public final static Long SESSION_LIFETIME = 3600000L; //1 hour
}

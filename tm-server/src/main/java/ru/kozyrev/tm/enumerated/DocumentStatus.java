package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;

@Getter
@RequiredArgsConstructor
public enum DocumentStatus {
    PLANNED("Запланированно", 1, "plan"),
    IN_PROGRESS("В процессе", 2, "progress"),
    READY("Готово", 3, "ready");

    @NotNull
    private final String displayName;

    private final Integer sortId;

    private final String shortName;

    @NotNull
    public static final DocumentStatus getType(@Nullable final String pType) throws UnknownTypeException {
        for (final DocumentStatus type : DocumentStatus.values()) {
            if (type.shortName.equals(pType)) {
                return type;
            }
        }
        throw new UnknownTypeException();
    }
}

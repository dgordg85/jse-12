package ru.kozyrev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.constant.TableConst;
import ru.kozyrev.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    protected static final String DB_NAME = TableConst.DB_NAME;

    @NotNull Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public abstract T fetch(@NotNull final ResultSet rs) throws Exception;

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract String getIdField();

    @Nullable
    @Override
    public List<T> findAll() throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @Nullable
    public final List<T> getList(
            @NotNull final ResultSet rs,
            @NotNull final PreparedStatement statement
    ) throws Exception {
        @NotNull final List<T> result = new ArrayList<>();
        while (rs.next()) {
            result.add(fetch(rs));
        }
        statement.close();
        rs.close();
        if (result.size() == 0) {
            return null;
        }
        return result;
    }

    @Nullable
    public final T fetch(
            @NotNull final ResultSet rs,
            @NotNull final PreparedStatement statement
    ) throws Exception {
        @Nullable final T entity = this.moveAndFetch(rs);
        rs.close();
        statement.close();
        return entity;
    }

    @Nullable
    public final T moveAndFetch(@NotNull final ResultSet rs) throws Exception {
        if (!rs.next()) {
            return null;
        }
        return fetch(rs);
    }
}

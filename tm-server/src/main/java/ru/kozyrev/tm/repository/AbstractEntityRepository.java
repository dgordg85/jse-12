package ru.kozyrev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IEntityRepository;
import ru.kozyrev.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@NoArgsConstructor
public abstract class AbstractEntityRepository<T extends AbstractEntity> extends AbstractRepository<T> implements IEntityRepository<T> {
    public AbstractEntityRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract T persist(@NotNull final T entity) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@NotNull final T entity) throws Exception;

    @Nullable
    @Override
    public abstract T fetch(@NotNull final ResultSet rs) throws Exception;

    @Nullable
    @Override
    public final T findOne(@NotNull final String id) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                getIdField() + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet rs = statement.executeQuery();
        return fetch(rs, statement);
    }

    @Override
    public final void remove(@NotNull final String id) throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                getIdField() + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }
}

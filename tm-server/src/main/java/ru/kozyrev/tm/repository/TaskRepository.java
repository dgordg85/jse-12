package ru.kozyrev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.constant.FieldConst;
import ru.kozyrev.tm.constant.TableConst;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskRepository extends AbstractObjectRepository<Task> implements ITaskRepository {
    @NotNull
    private static final String TABLE_NAME = TableConst.TASKS;

    @NotNull
    private static final String ID_FIELD = FieldConst.TASK_ID;

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public final Task persist(@NotNull final Task task) throws Exception {
        @NotNull final String sql = "INSERT INTO " +
                DB_NAME + "." +
                TABLE_NAME + " (" +
                ID_FIELD + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.DATE_START + ", " +
                FieldConst.DATE_FINISH + ", " +
                FieldConst.STATUS + ", " +
                FieldConst.PROJECT_ID + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getName());
        statement.setString(4, task.getDescription());
        statement.setDate(5, DateUtil.getSQLDate(task.getDateStart()));
        statement.setDate(6, DateUtil.getSQLDate(task.getDateFinish()));
        statement.setString(7, task.getStatus().toString());
        statement.setString(8, task.getProjectId());
        statement.executeUpdate();
        statement.close();
        return findOne(task.getId(), task.getUserId());
    }

    @Nullable
    @Override
    public final Task merge(@NotNull final Task task) throws Exception {
        @NotNull final String sql = "UPDATE " +
                DB_NAME + "." +
                TABLE_NAME + " SET " +
                ID_FIELD + "=?, " +
                FieldConst.USER_ID + "=?, " +
                FieldConst.PROJECT_ID + "=?, " +
                FieldConst.NAME + "=?, " +
                FieldConst.DESCRIPTION + "=?, " +
                FieldConst.DATE_START + "=?, " +
                FieldConst.DATE_FINISH + "=?, " +
                FieldConst.STATUS + "=? WHERE " +
                ID_FIELD + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setDate(6, DateUtil.getSQLDate(task.getDateStart()));
        statement.setDate(7, DateUtil.getSQLDate(task.getDateFinish()));
        statement.setString(8, task.getStatus().toString());
        statement.setString(9, task.getId());
        statement.executeUpdate();
        statement.close();
        return findOne(task.getId(), task.getUserId());
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Override
    public final void removeAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public final List<Task> findAllByNum(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Task> findAllByDateStart(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.DATE_START + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Task> findAllByDateFinish(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.DATE_FINISH + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Task> findAllByStatus(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.PROJECT_ID + "=? AND " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.STATUS + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @NotNull
    public final Task fetch(@NotNull final ResultSet rs) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(rs.getString(ID_FIELD));
        task.setProjectId(rs.getString(FieldConst.PROJECT_ID));
        task.setName(rs.getString(FieldConst.NAME));
        task.setDescription(rs.getString(FieldConst.DESCRIPTION));
        task.setShortLink(rs.getInt(FieldConst.SHORT_LINK));
        task.setUserId(rs.getString(FieldConst.USER_ID));
        task.setDateStart(rs.getDate(FieldConst.DATE_START));
        task.setDateFinish(rs.getDate(FieldConst.DATE_FINISH));
        task.setStatus(DocumentStatus.valueOf(rs.getString(FieldConst.STATUS)));
        return task;
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
}

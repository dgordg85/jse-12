package ru.kozyrev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.constant.FieldConst;
import ru.kozyrev.tm.constant.TableConst;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

@Getter
@NoArgsConstructor
public final class SessionRepository extends AbstractEntityRepository<Session> implements ISessionRepository {
    @NotNull
    private static final String TABLE_NAME = TableConst.SESSIONS;

    @NotNull
    private static final String ID_FIELD = FieldConst.SESSION_ID;

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public final Session persist(@NotNull final Session session) throws Exception {
        @NotNull final String sql = "INSERT INTO " +
                DB_NAME + "." +
                TABLE_NAME + "(" +
                ID_FIELD + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.ROLE_TYPE + ", " +
                FieldConst.SIGNATURE + ", " +
                FieldConst.TIMESTAMP + ") VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRoleType().toString());
        statement.setString(4, session.getSignature());
        statement.setTimestamp(5, new Timestamp(session.getTimestamp()));
        statement.executeUpdate();
        statement.close();
        return findOne(session.getId());
    }

    @Nullable
    @Override
    public final Session merge(@NotNull final Session session) throws Exception {
        @NotNull final String sql = "UPDATE " +
                DB_NAME + "." +
                TABLE_NAME + " SET " +
                ID_FIELD + "=?, " +
                FieldConst.USER_ID + "=?, " +
                FieldConst.ROLE_TYPE + "=? WHERE " +
                ID_FIELD + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRoleType().toString());
        statement.setString(4, session.getId());
        statement.executeUpdate();
        statement.close();
        return findOne(session.getId());
    }

    @Nullable
    @Override
    public final Session getSessionByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return fetch(rs, statement);
    }

    @NotNull
    public final Session fetch(@NotNull final ResultSet rs) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(rs.getString(ID_FIELD));
        session.setUserId(rs.getString(FieldConst.USER_ID));
        session.setRoleType(RoleType.valueOf(rs.getString(FieldConst.ROLE_TYPE)));
        session.setSignature(rs.getString(FieldConst.SIGNATURE));
        session.setTimestamp(rs.getTimestamp(FieldConst.TIMESTAMP).getTime());
        return session;
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
}
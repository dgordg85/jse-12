package ru.kozyrev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.constant.FieldConst;
import ru.kozyrev.tm.constant.TableConst;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractObjectRepository<Project> implements IProjectRepository {
    @NotNull
    private static final String TABLE_NAME = TableConst.PROJECTS;

    @NotNull
    private static final String ID_FIELD = FieldConst.PROJECT_ID;

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public final Project persist(@NotNull final Project project) throws Exception {
        @NotNull final String sql = "INSERT INTO " +
                DB_NAME + "." +
                TABLE_NAME + " (" +
                ID_FIELD + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.DATE_START + ", " +
                FieldConst.DATE_FINISH + ", " +
                FieldConst.STATUS + ") VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, DateUtil.getSQLDate(project.getDateStart()));
        statement.setDate(6, DateUtil.getSQLDate(project.getDateFinish()));
        statement.setString(7, project.getStatus().toString());
        statement.executeUpdate();
        statement.close();
        return findOne(project.getId(), project.getUserId());
    }

    @Nullable
    @Override
    public final Project merge(@NotNull final Project project) throws Exception {
        @NotNull final String sql = "UPDATE " +
                DB_NAME + "." +
                TABLE_NAME + " SET " +
                ID_FIELD + "=?, " +
                FieldConst.USER_ID + "=?, " +
                FieldConst.NAME + "=?, " +
                FieldConst.DESCRIPTION + "=?, " +
                FieldConst.DATE_START + "=?, " +
                FieldConst.DATE_FINISH + "=?, " +
                FieldConst.STATUS + "=? WHERE " +
                ID_FIELD + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, DateUtil.getSQLDate(project.getDateStart()));
        statement.setDate(6, DateUtil.getSQLDate(project.getDateFinish()));
        statement.setString(7, project.getStatus().toString());
        statement.setString(8, project.getId());
        statement.executeUpdate();
        statement.close();
        return findOne(project.getId(), project.getUserId());
    }

    @Nullable
    @Override
    public final List<Project> findAllByNum(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Project> findAllByDateStart(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.DATE_START + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Project> findAllByDateFinish(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.DATE_FINISH + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<Project> findAllByStatus(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.USER_ID + "=? ORDER BY " +
                FieldConst.STATUS + " " +
                direction + ", " +
                FieldConst.SHORT_LINK + " " +
                direction;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @NotNull
    public final Project fetch(@NotNull final ResultSet rs) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(rs.getString(ID_FIELD));
        project.setName(rs.getString(FieldConst.NAME));
        project.setDescription(rs.getString(FieldConst.DESCRIPTION));
        project.setShortLink(rs.getInt(FieldConst.SHORT_LINK));
        project.setUserId(rs.getString(FieldConst.USER_ID));
        project.setDateStart(rs.getDate(FieldConst.DATE_START));
        project.setDateFinish(rs.getDate(FieldConst.DATE_FINISH));
        project.setStatus(DocumentStatus.valueOf(rs.getString(FieldConst.STATUS)));
        return project;
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
}

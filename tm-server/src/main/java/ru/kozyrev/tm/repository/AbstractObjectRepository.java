package ru.kozyrev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IObjectRepository;
import ru.kozyrev.tm.constant.FieldConst;
import ru.kozyrev.tm.entity.AbstractObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectRepository<T extends AbstractObject> extends AbstractRepository<T> implements IObjectRepository<T> {
    public AbstractObjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract T persist(@NotNull final T object) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@NotNull final T object) throws Exception;

    @Nullable
    @Override
    public abstract T fetch(@NotNull final ResultSet rs) throws Exception;

    @Nullable
    @Override
    public final T findOne(@NotNull final String id, @NotNull final String userId) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                getIdField() + "=? AND " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return fetch(rs, statement);
    }

    @Nullable
    @Override
    public final List<T> findAll() throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " ORDER BY " +
                FieldConst.SHORT_LINK + " ASC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Nullable
    @Override
    public final List<T> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    @Override
    public final void removeAll() throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        resetIncrement();
        statement.close();
    }

    @Override
    public final void remove(@NotNull final String id, @NotNull final String userId) throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                getIdField() + "=? AND " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public final void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String sql = "DELETE FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public final T findOneByShortLink(@NotNull final Integer shortLink, @NotNull final String userId) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                FieldConst.SHORT_LINK + "=? AND " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, shortLink);
        statement.setString(2, userId);
        @NotNull final ResultSet rs = statement.executeQuery();
        return fetch(rs, statement);
    }

    @Nullable
    @Override
    public final List<T> findWord(@NotNull final String word, @NotNull final String userId) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                getTableName() + " WHERE " +
                FieldConst.USER_ID + "=? AND (" +
                FieldConst.NAME + " LIKE " +
                "? OR " +
                FieldConst.DESCRIPTION + " LIKE " +
                "?) ORDER BY " +
                FieldConst.SHORT_LINK + " ASC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, "%" + word + "%");
        statement.setString(3, "%" + word + "%");
        @NotNull final ResultSet rs = statement.executeQuery();
        return getList(rs, statement);
    }

    private final void resetIncrement() throws Exception {
        @NotNull final String sql = "ALTER TABLE " +
                DB_NAME + "." +
                getTableName() + " AUTO_INCREMENT=1";
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }
}

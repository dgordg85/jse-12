package ru.kozyrev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.constant.FieldConst;
import ru.kozyrev.tm.constant.TableConst;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Getter
@NoArgsConstructor
public final class UserRepository extends AbstractEntityRepository<User> implements IUserRepository {
    @NotNull
    private static final String TABLE_NAME = TableConst.USERS;

    @NotNull
    private static final String ID_FIELD = FieldConst.USER_ID;

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public final User persist(@NotNull final User user) throws Exception {
        @NotNull final String sql = "INSERT INTO " +
                DB_NAME + "." +
                TABLE_NAME + " (" +
                ID_FIELD + ", " +
                FieldConst.LOGIN + ", " +
                FieldConst.PASSWORD_HASH + ", " +
                FieldConst.ROLE_TYPE + ") VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRoleType().toString());
        statement.executeUpdate();
        statement.close();
        return findOne(user.getId());
    }

    @Nullable
    @Override
    public final User merge(@NotNull final User user) throws Exception {
        @NotNull final String sql = "UPDATE " +
                DB_NAME + "." +
                TABLE_NAME + " SET " +
                ID_FIELD + "=?, " +
                FieldConst.LOGIN + "=?, " +
                FieldConst.PASSWORD_HASH + "=?, " +
                FieldConst.ROLE_TYPE + "=? WHERE " +
                ID_FIELD + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRoleType().toString());
        statement.setString(5, user.getId());
        statement.executeUpdate();
        statement.close();
        return findOne(user.getId());
    }

    @Nullable
    @Override
    public final User getUserByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sql = "SELECT * FROM " +
                DB_NAME + "." +
                TABLE_NAME + " WHERE " +
                FieldConst.LOGIN + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet rs = statement.executeQuery();
        return fetch(rs, statement);
    }

    @NotNull
    public final User fetch(@NotNull final ResultSet rs) throws Exception {
        @NotNull final User user = new User();
        user.setId(rs.getString(ID_FIELD));
        user.setLogin(rs.getString(FieldConst.LOGIN));
        user.setPasswordHash(rs.getString(FieldConst.PASSWORD_HASH));
        user.setRoleType(RoleType.valueOf(rs.getString(FieldConst.ROLE_TYPE)));
        return user;
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
}

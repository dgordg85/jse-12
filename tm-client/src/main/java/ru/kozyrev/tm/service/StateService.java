package ru.kozyrev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IStateService;
import ru.kozyrev.tm.endpoint.Column;
import ru.kozyrev.tm.endpoint.Direction;
import ru.kozyrev.tm.endpoint.Session;

@Getter
@Setter
public final class StateService implements IStateService {
    @Nullable
    private Session session = null;

    @NotNull
    private Column projectColumn = Column.NUM;

    @NotNull
    private Column taskColumn = Column.NUM;

    @NotNull
    private Direction projectDirection = Direction.ASC;

    @NotNull
    private Direction taskDirection = Direction.ASC;

    @Override
    public final void clearSession() {
        session = null;
    }
}

package ru.kozyrev.tm.command;

import lombok.Getter;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.EndpointLocator;
import ru.kozyrev.tm.api.service.IStateService;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.endpoint.*;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;
import ru.kozyrev.tm.util.DateUtil;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractCommand {
    @NonNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    protected Boolean secure = false;

    @NotNull
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    protected TaskEndpoint taskEndpoint;

    @NotNull
    protected UserEndpoint userEndpoint;

    @NotNull
    protected AdminEndpoint adminEndpoint;

    @NotNull
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    protected ITerminalService terminalService;

    @NotNull
    protected IStateService stateService;

    public final void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.projectEndpoint = endpointLocator.getProjectEndpoint();
        this.taskEndpoint = endpointLocator.getTaskEndpoint();
        this.userEndpoint = endpointLocator.getUserEndpoint();
        this.adminEndpoint = endpointLocator.getAdminEndpoint();
        this.sessionEndpoint = endpointLocator.getSessionEndpoint();
    }

    public final void setTerminalService(@NotNull final ITerminalService terminalService) {
        this.terminalService = terminalService;
    }

    public final void setStateService(@NotNull final IStateService stateService) {
        this.stateService = stateService;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract Integer getSortId();

    public final <T extends AbstractObject> void printList(@Nullable final List<T> list) throws Exception {
        if (list == null || list.size() == 0) {
            System.out.println("[EMPTY]");
            return;
        }
        int count = 1;

        for (final T entity : list) {
            if (entity.getStatus() == null) {
                throw new UnknownTypeException();
            }
            System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s - %s\n",
                    count++,
                    entity.getName(),
                    entity.getShortLink(),
                    entity.getDescription(),
                    DateUtil.getDateXML(entity.getDateStart()),
                    DateUtil.getDateXML(entity.getDateFinish()),
                    adminEndpoint.adminGetDocumentStatusStr(entity.getStatus().value())
            );
        }
    }
}

package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 48;

    public TaskRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session session = stateService.getSession();

        System.out.println("[TASK DELETE]\nENTER ID:");
        @NotNull final String taskNum = terminalService.nextLine();

        taskEndpoint.taskRemoveByShortLink(session, taskNum);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

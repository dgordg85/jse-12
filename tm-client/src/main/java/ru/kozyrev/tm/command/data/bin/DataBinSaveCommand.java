package ru.kozyrev.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class DataBinSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 60;

    public DataBinSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data in Binary format to file";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SERIALIZATION BIN SAVE]");
        @Nullable final Session session = stateService.getSession();
        adminEndpoint.adminBinSave(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

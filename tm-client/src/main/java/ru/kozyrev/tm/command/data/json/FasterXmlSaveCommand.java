package ru.kozyrev.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class FasterXmlSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 65;

    public FasterXmlSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-json-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data in JSON format to file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML JSON SAVE]");
        @Nullable final Session session = stateService.getSession();
        adminEndpoint.adminJsonSaveFasterXML(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

package ru.kozyrev.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class JaxBLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 70;

    public JaxBLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-xml-JaxB";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from XML file by Jax-B";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[Jax-B XML LOAD]");
        @Nullable final Session session = stateService.getSession();
        adminEndpoint.adminXmlLoadJaxB(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

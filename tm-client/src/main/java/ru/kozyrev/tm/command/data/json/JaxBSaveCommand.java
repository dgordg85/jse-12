package ru.kozyrev.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;


public final class JaxBSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 67;

    public JaxBSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-json-JaxB";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data from JSON file by Jax-B";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[Jax-B JSON SAVE]");
        @Nullable final Session session = stateService.getSession();
        adminEndpoint.adminJsonSaveJaxB(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

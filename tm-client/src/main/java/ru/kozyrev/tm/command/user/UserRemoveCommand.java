package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class UserRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 11;

    public UserRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session session = stateService.getSession();

        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        @NotNull final String answer = terminalService.nextLine();
        if ("yes".equals(answer.toLowerCase())) {
            userEndpoint.userRemove(session, session.getUserId());
            System.out.println("[USER DELETE!]");
            stateService.clearSession();
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}

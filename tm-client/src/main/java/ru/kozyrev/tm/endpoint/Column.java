
package ru.kozyrev.tm.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for column.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="column"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NUM"/&gt;
 *     &lt;enumeration value="DATE_BEGIN"/&gt;
 *     &lt;enumeration value="DATE_FINISH"/&gt;
 *     &lt;enumeration value="STATUS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "column")
@XmlEnum
public enum Column {

    NUM,
    DATE_BEGIN,
    DATE_FINISH,
    STATUS;

    public String value() {
        return name();
    }

    public static Column fromValue(final String v) {
        return valueOf(v);
    }

}

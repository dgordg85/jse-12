package ru.kozyrev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-06-01T00:27:02.803+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.tm.kozyrev.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectRemoveAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveAuth")
    @ResponseWrapper(localName = "projectRemoveAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveAuthResponse")
    void projectRemoveAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindOneAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindOneAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindOneAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectFindOneAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindOneAuth")
    @ResponseWrapper(localName = "projectFindOneAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindOneAuthResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.kozyrev.tm.endpoint.Project projectFindOneAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveByShortLinkRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveByShortLinkResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveByShortLink/Fault/Exception")})
    @RequestWrapper(localName = "projectRemoveByShortLink", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveByShortLink")
    @ResponseWrapper(localName = "projectRemoveByShortLinkResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveByShortLinkResponse")
    void projectRemoveByShortLink(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindWordRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindWordResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindWord/Fault/Exception")})
    @RequestWrapper(localName = "projectFindWord", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindWord")
    @ResponseWrapper(localName = "projectFindWordResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindWordResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.kozyrev.tm.endpoint.Project> projectFindWord(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAllAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAllAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectRemoveAllAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectRemoveAllAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveAllAuth")
    @ResponseWrapper(localName = "projectRemoveAllAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectRemoveAllAuthResponse")
    void projectRemoveAllAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectPersistAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectPersistAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectPersistAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectPersistAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectPersistAuth")
    @ResponseWrapper(localName = "projectPersistAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectPersistAuthResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.kozyrev.tm.endpoint.Project projectPersistAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Project arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectFindAllAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindAllAuth")
    @ResponseWrapper(localName = "projectFindAllAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindAllAuthResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.kozyrev.tm.endpoint.Project> projectFindAllAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectGetEntityIdByShortLinkRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectGetEntityIdByShortLinkResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectGetEntityIdByShortLink/Fault/Exception")})
    @RequestWrapper(localName = "projectGetEntityIdByShortLink", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectGetEntityIdByShortLink")
    @ResponseWrapper(localName = "projectGetEntityIdByShortLinkResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectGetEntityIdByShortLinkResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.lang.String projectGetEntityIdByShortLink(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectMergeAuthRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectMergeAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectMergeAuth/Fault/Exception")})
    @RequestWrapper(localName = "projectMergeAuth", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectMergeAuth")
    @ResponseWrapper(localName = "projectMergeAuthResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectMergeAuthResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.kozyrev.tm.endpoint.Project projectMergeAuth(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Project arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllBySortingRequest", output = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllBySortingResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kozyrev.ru/ProjectEndpoint/projectFindAllBySorting/Fault/Exception")})
    @RequestWrapper(localName = "projectFindAllBySorting", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindAllBySorting")
    @ResponseWrapper(localName = "projectFindAllBySortingResponse", targetNamespace = "http://endpoint.tm.kozyrev.ru/", className = "ru.kozyrev.tm.endpoint.ProjectFindAllBySortingResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.kozyrev.tm.endpoint.Project> projectFindAllBySorting(
            @WebParam(name = "arg0", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Session arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Column arg1,
            @WebParam(name = "arg2", targetNamespace = "")
                    ru.kozyrev.tm.endpoint.Direction arg2
    ) throws Exception_Exception;
}

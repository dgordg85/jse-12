
package ru.kozyrev.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for projectFindAllBySorting complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="projectFindAllBySorting"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://endpoint.tm.kozyrev.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="arg1" type="{http://endpoint.tm.kozyrev.ru/}column" minOccurs="0"/&gt;
 *         &lt;element name="arg2" type="{http://endpoint.tm.kozyrev.ru/}direction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectFindAllBySorting", propOrder = {
        "arg0",
        "arg1",
        "arg2"
})
public class ProjectFindAllBySorting {

    protected Session arg0;
    @XmlSchemaType(name = "string")
    protected Column arg1;
    @XmlSchemaType(name = "string")
    protected Direction arg2;

    /**
     * Gets the value of the arg0 property.
     *
     * @return
     *     possible object is
     *     {@link Session }
     *
     */
    public Session getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     *
     * @param value
     *     allowed object is
     *     {@link Session }
     *
     */
    public void setArg0(final Session value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     *
     * @return
     *     possible object is
     *     {@link Column }
     *
     */
    public Column getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     *
     * @param value
     *     allowed object is
     *     {@link Column }
     *
     */
    public void setArg1(final Column value) {
        this.arg1 = value;
    }

    /**
     * Gets the value of the arg2 property.
     *
     * @return
     *     possible object is
     *     {@link Direction }
     *
     */
    public Direction getArg2() {
        return arg2;
    }

    /**
     * Sets the value of the arg2 property.
     *
     * @param value
     *     allowed object is
     *     {@link Direction }
     *
     */
    public void setArg2(final Direction value) {
        this.arg2 = value;
    }

}

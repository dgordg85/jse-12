package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.endpoint.Column;
import ru.kozyrev.tm.endpoint.Direction;
import ru.kozyrev.tm.endpoint.Session;

public interface IStateService {
    @NotNull
    Column getProjectColumn();

    void setProjectColumn(@NotNull Column column);

    @NotNull
    Direction getProjectDirection();

    void setProjectDirection(@NotNull Direction direction);

    @NotNull
    Column getTaskColumn();

    void setTaskColumn(@NotNull Column column);

    @NotNull
    Direction getTaskDirection();

    void setTaskDirection(@NotNull Direction direction);

    @Nullable
    Session getSession();

    void setSession(@NotNull Session session);

    void clearSession();
}

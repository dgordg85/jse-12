package ru.kozyrev.tm.exception.entity;

public final class EmptyStringException extends Exception {
    public EmptyStringException() {
        super("Empty string!");
    }
}

package ru.kozyrev.tm.exception.user;

public final class LoginEmptyException extends Exception {
    public LoginEmptyException() {
        super("Login empty!");
    }
}

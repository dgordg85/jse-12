package ru.kozyrev.tm.exception.entity;

public final class DescriptionException extends Exception {
    public DescriptionException() {
        super("Wrong Description!");
    }
}
